<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) return;


$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'ajouter_lien_facture' => 'Ajouter cette facture',

	// E
	'explication_conditions' => 'Indiquez les conditions de règlement proposées pour cette facture ou ce devis',
	'explication_date_facture' => 'Saisissez la date de facture ou corrigez celle proposée par défaut',
	'explication_delais_validite' => 'Indiquez un nombre de jours de validité pour le devis ou la facture',
	'explication_fin_validite' => 'Indiquez une date de fin de validité pour cette facture ou ce devis',
	'explication_id_organisation' => 'Saisissez l\'organisation destinataire du devis ou de la facture',
	'explication_id_organisation_emettrice' => 'Sélectionnez l\'organisation émettrice de la facture ou du devis',
	'explication_libelle_facture' => 'Saisissez un libellé explicite pour la facture ou le devis',
	'explication_montant' => 'Indiquez le montant total de la facture ou corrigez celui calculé par SPIP à partir des lignes de factures',
	'explication_nota_bene' => 'Indiquez un nota-bene qui apparaitra à la fin de la facture ou du devis (équivalent des « mentions manuscrites »)',
	'explication_num_devis' => 'Indiquez une référence à un devis précédent (le cas échéant)',
	'explication_num_facture' => 'Corrigez l\'identifiant de la facture ou corriges celui proposé par défaut',
	'explication_quantite' => 'Indiquez une quantité globale relative à cette facture ou ce devis (en général un nombre d\'heures ou de jours)',
	'explication_reglement' => 'Indiquez un mode de règlement pour cette facture ou ce devis',
	'explication_unite' => 'Indiquez une unité relative à cette facture ou ce devis (en général des heures ou des jours)',

	// I
	'icone_creer_facture' => 'Créer une facture',
	'icone_modifier_facture' => 'Modifier cette facture',
	'info_1_facture' => 'Une facture',
	'info_aucun_facture' => 'Aucune facture',
	'info_factures_auteur' => 'Les factures de cet auteur',
	'info_nb_factures' => '@nb@ factures',

	// L
	'label_conditions' => 'Conditions commerciales',
	'label_date_facture' => 'Date de facturation',
	'label_delais_validite' => 'Delais de validité',
	'label_fin_validite' => 'Fin de validité',
	'label_id_organisation' => 'Organisation facturée',
	'label_id_organisation_emettrice' => 'Organisation émettrice',
	'label_libelle_facture' => 'Libellé',
	'label_montant' => 'Montant global (calculé)',
	'label_nota_bene' => 'Nota bene',
	'label_num_devis' => 'Numéro du devis lié',
	'label_num_facture' => 'Numéro de la facture',
	'label_quantite' => 'Quantité globale',
	'label_reference_abbr' => 'Réf.',
	'label_reglement' => 'Mode de règlement',
	'label_unite' => 'Unité vendue',

	// R
	'retirer_lien_facture' => 'Retirer cette facture',
	'retirer_tous_liens_factures' => 'Retirer toutes les factures',

	// T
	'texte_ajouter_facture' => 'Ajouter une facture',
	'texte_changer_statut_facture' => 'Cette facture est :',
	'texte_creer_associer_facture' => 'Créer et associer une facture',
	'titre_facture' => 'Facture',
	'titre_factures' => 'Factures',
	'titre_factures_rubrique' => 'Factures de la rubrique',
	'titre_langue_facture' => 'Langue de cette facture',
	'titre_logo_facture' => 'Logo de cette facture',
);

?>
