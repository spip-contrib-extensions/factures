<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// F
	'factures_description' => 'Factures & devis permet d\'éditer, imprimer, archiver facilement vos devis et factures.',
	'factures_nom' => 'Factures & devis',
	'factures_slogan' => 'Facturer et faire des devis avec SPIP',
);

?>